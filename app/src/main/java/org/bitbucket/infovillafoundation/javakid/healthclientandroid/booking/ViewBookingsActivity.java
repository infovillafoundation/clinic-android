package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ViewBookingsActivity extends Activity {
    private ListView allBookings;
    private Button back;
    private BookingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bookings);

        allBookings = (ListView) findViewById(R.id.bookings);
        back = (Button) findViewById(R.id.back);

        allBookings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BookingDataContainer.currentBooking = adapter.getItem(i);
                Intent intent = new Intent(ViewBookingsActivity.this, BookingDetailsActivity.class);
                startActivityForResult(intent, 15);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewBookingsActivity.this, BookingMainActivity.class);
            }
        });

        populateList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            populateList();
        }
    }

    private void populateList() {
        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8085")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        HealthService healthService = retrofit.create(HealthService.class);


        Call<List<Booking>> callGetAll = healthService.getAllBookings();
        callGetAll.enqueue(new Callback<List<Booking>>() {
            @Override
            public void onResponse(Response<List<Booking>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Booking> bookings = response.body();
                    adapter = new BookingAdapter(ViewBookingsActivity.this, bookings);
                    allBookings.setAdapter(adapter);
                } else {
                    Toast.makeText(ViewBookingsActivity.this, "Wait for a while", Toast.LENGTH_LONG).show();
                    ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                    Runnable task = new Runnable() {
                        public void run() {
                            populateList();
                        }
                    };
                    worker.schedule(task, 5, TimeUnit.SECONDS);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Toast.makeText(ViewBookingsActivity.this, "Wait for a while", Toast.LENGTH_LONG).show();
                ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                Runnable task = new Runnable() {
                    public void run() {
                        populateList();
                    }
                };
                worker.schedule(task, 5, TimeUnit.SECONDS);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_bookings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
