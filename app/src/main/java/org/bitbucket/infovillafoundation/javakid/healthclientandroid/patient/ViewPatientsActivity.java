package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.MainActivity;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ViewPatientsActivity extends AppCompatActivity {

    private ListView allPatients;
    private Button back;
    private PatientAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_patients);
        back = (Button) findViewById(R.id.back);
        allPatients = (ListView) findViewById(R.id.all_patients);

        populateList();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewPatientsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        allPatients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Patient patient = adapter.getItem(position);
                PatientDataContainer.currentPatient = patient;
                Intent intent = new Intent(ViewPatientsActivity.this, PatientDetailsActivity.class);
                startActivityForResult(intent, 15);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            populateList();
        }
    }

    private void populateList() {
        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8085")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        HealthService healthService = retrofit.create(HealthService.class);


        Call<List<Patient>> callGetAll = healthService.getAllPatients();
        callGetAll.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Response<List<Patient>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Patient> patients = response.body();
                    adapter = new PatientAdapter(ViewPatientsActivity.this, patients);
                    allPatients.setAdapter(adapter);
                }
                else {
                    Toast.makeText(ViewPatientsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    Log.d("failed", response.message());
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Toast.makeText(ViewPatientsActivity.this, "Wait for a while", Toast.LENGTH_LONG).show();
                ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                Runnable task = new Runnable() {
                    public void run() {
                        populateList();
                    }
                };
                worker.schedule(task, 5, TimeUnit.SECONDS);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_bookings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
