package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;

import java.util.List;

/**
 * Created by aztunwin on 10/12/15.
 */
public class PatientAdapter extends ArrayAdapter<Patient> {

    private Context context;
    private List<Patient> patients;

    public PatientAdapter(Context context, List<Patient> objects) {
        super(context, R.layout.patient_item, objects);
        this.context = context;
        this.patients = objects;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.patient_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        Patient patient = patients.get(position);
        if (patient.getGender() == Gender.Male) {
            holder.getGenderImage().setImageResource(R.drawable.male);
        }
        else {
            holder.getGenderImage().setImageResource(R.drawable.female);
        }

        holder.getName().setText(patient.getName());
        holder.getNirc().setText(patient.getNirc());

        return view;
    }

    static class ViewHolder {

        private ImageView genderImage;
        private TextView name;
        private TextView nirc;
        private View view;

        public ViewHolder(View view) {
            this.view = view;
            genderImage = (ImageView) view.findViewById(R.id.gender_image);
            name = (TextView) view.findViewById(R.id.name);
            nirc = (TextView) view.findViewById(R.id.nirc);
        }

        public ImageView getGenderImage() {
            return genderImage;
        }

        public void setGenderImage(ImageView genderImage) {
            this.genderImage = genderImage;
        }

        public TextView getName() {
            return name;
        }

        public void setName(TextView name) {
            this.name = name;
        }

        public TextView getNirc() {
            return nirc;
        }

        public void setNirc(TextView nirc) {
            this.nirc = nirc;
        }
    }
}
