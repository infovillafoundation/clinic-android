package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class PatientEditDetailsActivity extends AppCompatActivity {
    private EditText name;
    private RadioGroup gender;
    private RadioButton male;
    private RadioButton female;
    private EditText nirc;
    private EditText email;
    private EditText dateOfBirth;
    private EditText phoneNumber;
    private Button update;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_edit_details);

        name = (EditText) findViewById(R.id.nameField);
        gender = (RadioGroup) findViewById(R.id.genderGroup);
        male = (RadioButton) findViewById(R.id.maleButton);
        female = (RadioButton) findViewById(R.id.femaleButton);
        nirc = (EditText) findViewById(R.id.nircField);
        email = (EditText) findViewById(R.id.emailField);
        dateOfBirth = (EditText) findViewById(R.id.dobField);
        phoneNumber = (EditText) findViewById(R.id.phoneNumberField);
        update = (Button) findViewById(R.id.finish);
        cancel = (Button) findViewById(R.id.cancel);

        final Patient patient = PatientDataContainer.currentPatient;

        name.setText(PatientDataContainer.currentPatient.getName());
        nirc.setText(PatientDataContainer.currentPatient.getNirc());
        if (patient.getGender() == Gender.Male) {
            gender.check(R.id.maleButton);
        }
        else {
            gender.check(R.id.femaleButton);
        }

        email.setText(PatientDataContainer.currentPatient.getEmail());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        dateOfBirth.setText(format.format(PatientDataContainer.currentPatient.getDateOfBirth()));
        phoneNumber.setText(PatientDataContainer.currentPatient.getPhoneNumber());

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientEditDetailsActivity.this.finish();
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8085")
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient)
                        .build();

                HealthService healthService = retrofit.create(HealthService.class);

                String nameUpdate = name.getText().toString();
                String nircUpdate = nirc.getText().toString();
                String emailUpdate = email.getText().toString();
                String dobUpdate = dateOfBirth.getText().toString();
                String phoneNumberUpdate = phoneNumber.getText().toString();
                int selected = gender.getCheckedRadioButtonId();
                Gender genderUpdate = selected == male.getId() ? Gender.Male : Gender.Female;
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date dobDateUpdate = null;
                try {
                    dobDateUpdate = format.parse(dobUpdate);
                } catch (ParseException e) {

                }

                Map<String, String> params = new HashMap<String, String>();

                if (!nameUpdate.equals(patient.getName())) {
                    params.put("name", nameUpdate);
                }

                if (!nircUpdate.equals(patient.getNirc())) {
                    params.put("nirc", nircUpdate);
                }

                if (!genderUpdate.equals(patient.getGender())) {
                    if (genderUpdate == Gender.Male)
                        params.put("gender", "male");
                    else
                        params.put("gender", "female");
                }

                if (!emailUpdate.equals(patient.getEmail())) {
                    params.put("email", emailUpdate);
                }

                if (!dobUpdate.equals(patient.getDateOfBirth())) {
                    params.put("dob", dobUpdate);
                }

                if (!phoneNumberUpdate.equals(patient.getPhoneNumber())) {
                    params.put("phonenum", phoneNumberUpdate);
                }

                PatientDataContainer.currentPatient.setName(nameUpdate);
                PatientDataContainer.currentPatient.setNirc(nircUpdate);
                PatientDataContainer.currentPatient.setGender(genderUpdate);
                PatientDataContainer.currentPatient.setEmail(emailUpdate);

                Date dobDateUpdate2 = null;
                try {
                    dobDateUpdate2 = format.parse(dobUpdate);
                } catch (ParseException e) {

                }
                PatientDataContainer.currentPatient.setDateOfBirth(dobDateUpdate2);
                PatientDataContainer.currentPatient.setPhoneNumber(phoneNumberUpdate);
                Call<Patient> callPut = healthService.updatePatient(patient.getId(), params);
                callPut.enqueue(new Callback<Patient>() {
                    @Override
                    public void onResponse(Response<Patient> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            Intent intent = new Intent(PatientEditDetailsActivity.this, ViewPatientsActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(PatientEditDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                            Log.d("small fail", response.message());
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(PatientEditDetailsActivity.this, "Failed horribly", Toast.LENGTH_SHORT).show();
                        Log.d("big fail", throwable.getMessage());
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_edit_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
