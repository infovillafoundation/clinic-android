package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class BookingDetailsActivity extends Activity {
    private TextView bookingPatient;
    private TextView bookingDoctor;
    private TextView bookingStart;
    private TextView bookingEnd;
    private TextView bookingDate;
    private Button cancel;
    private Button update;
    private Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);

        bookingPatient = (TextView) findViewById(R.id.bookingPatient);
        bookingDoctor = (TextView) findViewById(R.id.bookingDoctor);
        bookingStart = (TextView) findViewById(R.id.bookingStart);
        bookingEnd = (TextView) findViewById(R.id.bookingEnd);
        bookingDate = (TextView) findViewById(R.id.bookingDate);
        cancel = (Button) findViewById(R.id.bookingCancel);
        update = (Button) findViewById(R.id.bookingUpdate);
        delete = (Button) findViewById(R.id.bookingDelete);

        bookingPatient.setText(BookingDataContainer.currentBooking.getPatient());
        bookingDoctor.setText(BookingDataContainer.currentBooking.getDoctor());
        bookingStart.setText(BookingDataContainer.currentBooking.getStartHour());
        bookingEnd.setText(BookingDataContainer.currentBooking.getEndHour());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        bookingDate.setText(format.format(BookingDataContainer.currentBooking.getDate()));

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingDetailsActivity.this.finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingDetailsActivity.this, BookingEditDetailsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_booking_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
