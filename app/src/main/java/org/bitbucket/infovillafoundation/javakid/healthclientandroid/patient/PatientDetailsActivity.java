package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class PatientDetailsActivity extends AppCompatActivity {

    private TextView name;
    private TextView gender;
    private TextView nirc;
    private TextView email;
    private TextView dateOfBirth;
    private TextView phoneNumber;
    private Button back;
    private Button update;
    private Button delete;

    Patient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        name = (TextView) findViewById(R.id.name);
        gender = (TextView) findViewById(R.id.gender);
        nirc = (TextView) findViewById(R.id.nirc);
        email = (TextView) findViewById(R.id.email);
        dateOfBirth = (TextView) findViewById(R.id.dateOfBirth);
        phoneNumber = (TextView) findViewById(R.id.phoneNumber);
        back = (Button) findViewById(R.id.back);
        update = (Button) findViewById(R.id.update);
        delete = (Button) findViewById(R.id.delete);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientDetailsActivity.this.finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PatientDetailsActivity.this, PatientEditDetailsActivity.class);
                startActivity(intent);
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8085")
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient)
                        .build();

                HealthService healthService = retrofit.create(HealthService.class);


                Call<Patient> callDelete = healthService.deletePatient(patient.getId());
                callDelete.enqueue(new Callback<Patient>() {
                    @Override
                    public void onResponse(Response<Patient> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                            Runnable task = new Runnable() {
                                public void run() {
                                    goBack();
                                }
                            };
                            worker.schedule(task, 2, TimeUnit.SECONDS);
                        }
                        else {
                            Toast.makeText(PatientDetailsActivity.this, "Delete failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(PatientDetailsActivity.this, "Delete failed horribly", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        patient = PatientDataContainer.currentPatient;
        name.setText(patient.getName());
        if (patient.getGender() == Gender.Male) {
            gender.setText("Male");
        }
        else {
            gender.setText("Female");
        }
        nirc.setText(patient.getNirc());
        email.setText(patient.getEmail());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String dobString = format.format(patient.getDateOfBirth());
        dateOfBirth.setText(dobString);
        phoneNumber.setText(patient.getPhoneNumber());
    }

    private void goBack() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
