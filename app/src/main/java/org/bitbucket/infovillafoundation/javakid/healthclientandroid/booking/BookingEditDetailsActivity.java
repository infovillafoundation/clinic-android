package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;

public class BookingEditDetailsActivity extends Activity {
    private EditText patient; // Change to spinner?
    private EditText doctor; // Change to spinner?
    private EditText start; // Format/validation?
    private EditText end; // Format/validation?
    private EditText date; // Format/validation?
    private Button finish;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_edit_details);

        patient = (EditText) findViewById(R.id.bookingPatientField);
        doctor = (EditText) findViewById(R.id.bookingDoctorField);
        start = (EditText) findViewById(R.id.bookingStartField);
        end = (EditText) findViewById(R.id.bookingEndField);
        date = (EditText) findViewById(R.id.bookingDateField);
        finish = (Button) findViewById(R.id.finish);
        cancel = (Button) findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingEditDetailsActivity.this.finish();
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_booking_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
