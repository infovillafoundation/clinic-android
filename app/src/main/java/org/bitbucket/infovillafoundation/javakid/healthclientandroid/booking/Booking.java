package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.Patient;

import java.util.Date;

/**
 * Created by Way Yan on 12/13/2015.
 */

public class Booking {
    private String patient;
    private String doctor;
    private int startHour;
    private int endHour;
    private Date date;

    public Booking() {

    }

    public Booking(String patient, String doctor, int startHour, int endHour, Date date) {
        this.patient = patient;
        this.doctor = doctor;
        this.startHour = startHour;
        this.endHour = endHour;
        this.date = date;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
