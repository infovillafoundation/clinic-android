package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;

import java.util.Date;

/**
 * Created by Way Yan on 12/8/2015.
 */
public class Patient {
    private int id;
    private String name;
    private String nirc;
    private String email;
    private Gender gender;
    private Date dateOfBirth;
    private String phoneNumber;

    public Patient() {
    }

    public Patient(String name, String nirc, String email, Gender gender, Date dateOfBirth, String phoneNumber) {
        this.name = name;
        this.nirc = nirc;
        this.email = email;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNirc() {
        return nirc;
    }

    public void setNirc(String nirc) {
        this.nirc = nirc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
