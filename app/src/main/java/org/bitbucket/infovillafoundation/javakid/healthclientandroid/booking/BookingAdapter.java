package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Way Yan on 12/16/2015.
 */
public class BookingAdapter extends ArrayAdapter<Booking> {
    private Context context;
    private List<Booking> bookings;

    public BookingAdapter(Context context, List<Booking> objects) {
        super(context, R.layout.booking_item, objects);
        this.context = context;
        this.bookings = objects;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.doctor_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        Booking booking = bookings.get(position);

        holder.getPatient().setText(booking.getPatient());
        holder.getDoctor().setText(booking.getDoctor());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        holder.getDate().setText(format.format(booking.getDate()));
        return view;
    }

    static class ViewHolder {
        private TextView patient;
        private TextView doctor;
        private TextView date;
        private View view;

        public ViewHolder(View view) {
            this.view = view;
            patient = (TextView) view.findViewById(R.id.patientItem);
            doctor = (TextView) view.findViewById(R.id.doctorItem);
            date = (TextView) view.findViewById(R.id.dateItem);
        }
        
        public TextView getPatient() {
            return patient;
        }

        public void setPatient(TextView patient) {
            this.patient = patient;
        }

        public TextView getDoctor() {
            return doctor;
        }

        public void setDoctor(TextView doctor) {
            this.doctor = doctor;
        }

        public TextView getDate() {
            return date;
        }

        public void setDate(TextView date) {
            this.date = date;
        }
    }
}
