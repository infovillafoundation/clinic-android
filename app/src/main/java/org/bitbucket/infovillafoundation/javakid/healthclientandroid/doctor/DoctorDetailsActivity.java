package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.Patient;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.PatientDataContainer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class DoctorDetailsActivity extends AppCompatActivity {

    private TextView name;
    private TextView gender;
    private TextView email;
    private TextView dateOfBirth;
    private TextView licenseNumber;
    private TextView specialisation;
    private Button back;
    private Button update;
    private Button delete;

    Doctor doctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        name = (TextView) findViewById(R.id.name);
        gender = (TextView) findViewById(R.id.gender);
        email = (TextView) findViewById(R.id.email);
        dateOfBirth = (TextView) findViewById(R.id.dob);
        licenseNumber = (TextView) findViewById(R.id.license);
        specialisation = (TextView) findViewById(R.id.specialisation);
        back = (Button) findViewById(R.id.back);
        update = (Button) findViewById(R.id.update);
        delete = (Button) findViewById(R.id.delete);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoctorDetailsActivity.this.finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DoctorDetailsActivity.this, DoctorEditDetailsActivity.class);
                startActivity(intent);
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8085")
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient)
                        .build();

                HealthService healthService = retrofit.create(HealthService.class);


                Call<Doctor> callDelete = healthService.deleteDoctor(doctor.getId());
                callDelete.enqueue(new Callback<Doctor>() {
                    @Override
                    public void onResponse(Response<Doctor> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                            Runnable task = new Runnable() {
                                public void run() {
                                    goBack();
                                }
                            };
                            worker.schedule(task, 2, TimeUnit.SECONDS);
                        }
                        else {
                            Toast.makeText(DoctorDetailsActivity.this, "Delete failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(DoctorDetailsActivity.this, "Delete failed horribly", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        doctor = DoctorDataContainer.currentDoctor;
        name.setText(doctor.getName());
        if (doctor.getGender() == Gender.Male) {
            gender.setText("Male");
        }
        else {
            gender.setText("Female");
        }
        email.setText(doctor.getEmail());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String dobString = format.format(doctor.getDateOfBirth());
        Log.d("dobString:", dobString);
        Log.d("dateOfBirth", "" + dateOfBirth);
        dateOfBirth.setText(dobString);
        licenseNumber.setText(doctor.getLicenseNumber());
        specialisation.setText(doctor.getSpecialisation());
    }

    private void goBack() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
