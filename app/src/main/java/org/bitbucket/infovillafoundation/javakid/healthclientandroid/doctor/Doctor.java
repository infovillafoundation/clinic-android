package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;

import java.util.Date;

/**
 * Created by Way Yan on 12/11/2015.
 */
public class Doctor {
    private int id; // Use different id count
    private String name;
    private Gender gender;
    private String email;
    private Date dateOfBirth;
    private String phoneNumber;
    private String licenseNumber;
    private String specialisation;

    public Doctor() {

    }

    public Doctor(String name, Gender gender, String email, Date dateOfBirth, String phoneNumber, String licenseNumber, String specialisation) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.licenseNumber = licenseNumber;
        this.specialisation = specialisation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}
