package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ViewDoctorsActivity extends AppCompatActivity {
    private ListView allDoctors;
    private Button back;
    private DoctorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_doctors);

        allDoctors = (ListView) findViewById(R.id.doctors);
        back = (Button) findViewById(R.id.back);

        allDoctors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DoctorDataContainer.currentDoctor = adapter.getItem(i);
                Intent intent = new Intent(ViewDoctorsActivity.this, DoctorDetailsActivity.class);
                startActivityForResult(intent, 15);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewDoctorsActivity.this, DoctorMainActivity.class);
                startActivity(intent);
            }
        });

        populateList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            populateList();
        }
    }

    private void populateList() {
        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8085")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        HealthService healthService = retrofit.create(HealthService.class);


        Call<List<Doctor>> callGetAll = healthService.getAllDoctors();
        callGetAll.enqueue(new Callback<List<Doctor>>() {
            @Override
            public void onResponse(Response<List<Doctor>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Doctor> doctors = response.body();
                    adapter = new DoctorAdapter(ViewDoctorsActivity.this, doctors);
                    allDoctors.setAdapter(adapter);
                } else {
                    Toast.makeText(ViewDoctorsActivity.this, "Wait for a while", Toast.LENGTH_LONG).show();
                    ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                    Runnable task = new Runnable() {
                        public void run() {
                            populateList();
                        }
                    };
                    worker.schedule(task, 5, TimeUnit.SECONDS);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Toast.makeText(ViewDoctorsActivity.this, "Wait for a while", Toast.LENGTH_LONG).show();
                ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
                Runnable task = new Runnable() {
                    public void run() {
                        populateList();
                    }
                };
                worker.schedule(task, 5, TimeUnit.SECONDS);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_doctors, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
