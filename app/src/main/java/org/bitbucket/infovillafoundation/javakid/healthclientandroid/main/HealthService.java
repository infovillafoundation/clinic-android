package org.bitbucket.infovillafoundation.javakid.healthclientandroid.main;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking.Booking;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor.Doctor;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.Patient;

import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by Way Yan on 12/8/2015.
 */
public interface HealthService {

    @GET("/patients")
    Call<List<Patient>>
    getAllPatients();

    @GET("/patients/{id}")
    Call<Patient>
    getPatient(@Path("id") int id);

    @POST("/patients")
    Call<Patient>
    savePatient(@Body Patient patient);

    @DELETE("/patients/{id}")
    Call<Patient>
    deletePatient(@Path("id") int id);

    @PUT("/patients/{id}")
    Call<Patient>
    updatePatient(@Path("id") int id, @QueryMap Map<String, String> params);

    @GET("/doctors")
    Call<List<Doctor>>
    getAllDoctors();

    @GET("/doctors/{id}")
    Call<Doctor>
    getDoctor(@Path("id") int id);

    @POST("/doctors")
    Call<Doctor>
    saveDoctor(@Body Doctor doctor);

    @DELETE("/doctors/{id}")
    Call<Doctor>
    deleteDoctor(@Path("id") int id);

    @PUT("/doctors/{id}")
    Call<Doctor>
    updateDoctor(@Path("id") int id, @QueryMap Map<String, String> params);

    @GET("/bookings")
    Call<List<Booking>>
    getAllBookings();

    @GET("/bookings/{id}")
    Call<Booking>
    getBooking(@Path("id") int id);

    @POST("/bookings")
    Call<Booking>
    saveBooking(@Body Booking booking);

    @DELETE("/bookings/{id}")
    Call<Booking>
    deleteBooking(@Path("id") int id);

    @PUT("/bookings/{id}")
    Call<Booking>
    updateBooking(@Path("id") int id, @QueryMap Map<String, String> params);
}
