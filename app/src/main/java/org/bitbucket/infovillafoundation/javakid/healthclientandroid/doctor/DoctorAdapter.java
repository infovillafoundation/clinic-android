package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;

import java.util.List;

/**
 * Created by aztunwin on 11/12/15.
 */
public class DoctorAdapter extends ArrayAdapter<Doctor> {
    private Context context;
    private List<Doctor> doctors;

    public DoctorAdapter(Context context, List<Doctor> objects) {
        super(context, R.layout.doctor_item, objects);
        this.context = context;
        this.doctors = objects;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.doctor_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        Doctor doctor = doctors.get(position);
        if (doctor.getGender() == Gender.Male) {
            holder.getGenderImage().setImageResource(R.drawable.male);
        }
        else {
            holder.getGenderImage().setImageResource(R.drawable.female);
        }

        holder.getName().setText(doctor.getName());
        holder.getSpecialisation().setText(doctor.getSpecialisation());

        return view;
    }

    static class ViewHolder {

        private ImageView genderImage;
        private TextView name;
        private TextView specialisation;
        private View view;

        public ViewHolder(View view) {
            this.view = view;
            genderImage = (ImageView) view.findViewById(R.id.gender_image);
            name = (TextView) view.findViewById(R.id.name);
            specialisation = (TextView) view.findViewById(R.id.specialisation);
        }

        public ImageView getGenderImage() {
            return genderImage;
        }

        public void setGenderImage(ImageView genderImage) {
            this.genderImage = genderImage;
        }

        public TextView getName() {
            return name;
        }

        public void setName(TextView name) {
            this.name = name;
        }

        public TextView getSpecialisation() {
            return specialisation;
        }

        public void setSpecialisation(TextView specialisation) {
            this.name = specialisation;
        }
    }
}
