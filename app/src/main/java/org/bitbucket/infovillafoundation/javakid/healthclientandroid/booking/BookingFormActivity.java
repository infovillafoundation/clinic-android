package org.bitbucket.infovillafoundation.javakid.healthclientandroid.booking;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor.DoctorDataContainer;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.PatientDataContainer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class BookingFormActivity extends Activity {
    private EditText patient;
    private EditText doctor;
    private EditText start;
    private EditText end;
    private EditText date;
    private Button finish;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_form);

        patient = (EditText) findViewById(R.id.patientName);
        doctor = (EditText) findViewById(R.id.doctorName);
        start = (EditText) findViewById(R.id.startHour);
        end = (EditText) findViewById(R.id.endHour);
        date = (EditText) findViewById(R.id.date);
        finish = (Button) findViewById(R.id.finish);
        cancel = (Button) findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookingFormActivity.this.finish();
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasPatient = false;
                boolean hasDoctor = false;
                Booking booking = new Booking();
                for (String patientName : PatientDataContainer.patientNames) {
                    if (patient.equals(patientName)) {
                        booking.setPatient(patient.getText().toString());
                        hasPatient = true;
                    }
                }

                if (!hasPatient) {
                    patient.setError("No matching patient found");
                }

                for (String doctorName : DoctorDataContainer.doctorNames) {
                    if (doctor.equals(doctorName)) {
                        booking.setDoctor(doctor.getText().toString());
                        hasDoctor = true;
                    }
                }

                if (!hasDoctor) {
                    doctor.setError("No matching doctor found");
                }

                // Format and validation for start, end and date?
                booking.setStartHour(Integer.parseInt(start.getText().toString()));
                booking.setEndHour(Integer.parseInt(end.getText().toString()));
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    booking.setDate(format.parse(date.getText().toString()));
                } catch (ParseException exception) {
                    
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_booking_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
