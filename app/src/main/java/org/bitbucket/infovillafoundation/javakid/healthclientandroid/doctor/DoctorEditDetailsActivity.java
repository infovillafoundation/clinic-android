package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.Patient;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.PatientDataContainer;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient.ViewPatientsActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class DoctorEditDetailsActivity extends AppCompatActivity {
    private EditText name;
    private RadioGroup gender;
    private RadioButton male;
    private RadioButton female;
    private EditText email;
    private EditText dateOfBirth;
    private EditText licenseNumber;
    private Spinner specialisation;
    private Button update;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_edit_details);

        name = (EditText) findViewById(R.id.nameField);
        gender = (RadioGroup) findViewById(R.id.genderGroup);
        male = (RadioButton) findViewById(R.id.maleButton);
        female = (RadioButton) findViewById(R.id.femaleButton);
        email = (EditText) findViewById(R.id.emailField);
        dateOfBirth = (EditText) findViewById(R.id.dobField);
        licenseNumber = (EditText) findViewById(R.id.licenseField);
        specialisation = (Spinner) findViewById(R.id.specialisations);
        update = (Button) findViewById(R.id.finish);
        cancel = (Button) findViewById(R.id.cancel);

        final Doctor doctor = DoctorDataContainer.currentDoctor;

        name.setText(DoctorDataContainer.currentDoctor.getName());
        if (doctor.getGender() == Gender.Male) {
            gender.check(R.id.maleButton);
        }
        else {
            gender.check(R.id.femaleButton);
        }

        email.setText(DoctorDataContainer.currentDoctor.getEmail());
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        dateOfBirth.setText(format.format(DoctorDataContainer.currentDoctor.getDateOfBirth()));
        licenseNumber.setText(DoctorDataContainer.currentDoctor.getLicenseNumber());
        String[] specialisationsList = {"Imaging", "Pediatrics", "Orthopedic", "Emergency", "Obstetrics",
                "Internal Medicine", "Cardiology", "Neuroscience", "General Surgery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,
                specialisationsList);
        specialisation.setAdapter(adapter);
        

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoctorEditDetailsActivity.this.finish();
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8085")
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient)
                        .build();

                HealthService healthService = retrofit.create(HealthService.class);

                String nameUpdate = name.getText().toString();
                String emailUpdate = email.getText().toString();
                String dobUpdate = dateOfBirth.getText().toString();
                String licenseNumUpdate = licenseNumber.getText().toString();
                String specialisationUpdate;
                int index = specialisation.getSelectedItemPosition();
                switch (index) {
                    case 0:
                        specialisationUpdate = "Imaging";
                        break;
                    case 1:
                        specialisationUpdate = "Pediatrics";
                        break;
                    case 2:
                        specialisationUpdate = "Orthopedic";
                        break;
                    case 3:
                        specialisationUpdate = "Emergency";
                        break;
                    case 4:
                        specialisationUpdate = "Obstetrics";
                        break;
                    case 5:
                        specialisationUpdate = "Internal Medicine";
                        break;
                    case 6:
                        specialisationUpdate = "Cardiology";
                        break;
                    case 7:
                        specialisationUpdate = "Neuroscience";
                        break;
                    case 8:
                        specialisationUpdate = "General Surgery";
                        break;
                    default:
                        specialisationUpdate = "General Practice";
                }
                
                int selected = gender.getCheckedRadioButtonId();
                Gender genderUpdate = selected == male.getId() ? Gender.Male : Gender.Female;
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date dobDateUpdate = null;
                try {
                    dobDateUpdate = format.parse(dobUpdate);
                } catch (ParseException e) {

                }

                Map<String, String> params = new HashMap<String, String>();

                if (!nameUpdate.equals(doctor.getName())) {
                    params.put("name", nameUpdate);
                }

                if (!genderUpdate.equals(doctor.getGender())) {
                    if (genderUpdate == Gender.Male)
                        params.put("gender", "male");
                    else
                        params.put("gender", "female");
                }

                if (!emailUpdate.equals(doctor.getEmail())) {
                    params.put("email", emailUpdate);
                }

                if (!dobUpdate.equals(doctor.getDateOfBirth())) {
                    params.put("dob", dobUpdate);
                }

                if (licenseNumUpdate.equals(doctor.getLicenseNumber())) {
                    params.put("phonenum", licenseNumUpdate);
                }
                
                if (specialisationUpdate.equals(doctor.getSpecialisation())) {
                    params.put("specialisation", specialisationUpdate);
                }

                DoctorDataContainer.currentDoctor.setName(nameUpdate);
                DoctorDataContainer.currentDoctor.setGender(genderUpdate);
                DoctorDataContainer.currentDoctor.setEmail(emailUpdate);

                Date dobDateUpdate2 = null;
                try {
                    dobDateUpdate2 = format.parse(dobUpdate);
                } catch (ParseException e) {

                }
                DoctorDataContainer.currentDoctor.setDateOfBirth(dobDateUpdate2);
                DoctorDataContainer.currentDoctor.setPhoneNumber(licenseNumUpdate);
                DoctorDataContainer.currentDoctor.setSpecialisation(specialisationUpdate);

                Call<Doctor> callPut = healthService.updateDoctor(doctor.getId(), params);
                callPut.enqueue(new Callback<Doctor>() {
                    @Override
                    public void onResponse(Response<Doctor> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            Intent intent = new Intent(DoctorEditDetailsActivity.this, ViewDoctorsActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(DoctorEditDetailsActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                            Log.d("small fail", response.message());
                            Log.d("errno:", response.code() + "");
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        Toast.makeText(DoctorEditDetailsActivity.this, "Failed horribly", Toast.LENGTH_SHORT).show();
                        Log.d("big fail", throwable.getMessage());
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_edit_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
