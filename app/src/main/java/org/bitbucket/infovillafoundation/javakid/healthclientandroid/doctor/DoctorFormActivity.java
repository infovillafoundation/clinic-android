package org.bitbucket.infovillafoundation.javakid.healthclientandroid.doctor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class DoctorFormActivity extends AppCompatActivity {
    private EditText name;
    private RadioGroup gender;
    private RadioButton male;
    private RadioButton female;
    private EditText email;
    private EditText dateOfBirth;
    private EditText phoneNumber;
    private EditText licenseNumber;
    private Spinner specialisations;
    private Button back;
    private Button finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_form);

        name = (EditText) findViewById(R.id.name);
        gender = (RadioGroup) findViewById(R.id.gender);
        male = (RadioButton) findViewById(R.id.maleButton);
        female = (RadioButton) findViewById(R.id.femaleButton);
        email = (EditText) findViewById(R.id.email);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        licenseNumber = (EditText) findViewById(R.id.licenseNumber);
        specialisations = (Spinner) findViewById(R.id.specialisation);
        back = (Button) findViewById(R.id.back);
        finish = (Button) findViewById(R.id.finish);

        String[] specialisationsList = {"Imaging", "Pediatrics", "Orthopedic", "Emergency", "Obstetrics",
        "Internal Medicine", "Cardiology", "Neuroscience", "General Surgery"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item
        , specialisationsList);

        specialisations.setAdapter(adapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoctorFormActivity.this.finish();
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().isEmpty()) {
                    name.setError("Name cannot be empty");
                }
                else if (gender.getCheckedRadioButtonId() == -1) {
                    male.setError("Gender cannot be empty");
                }
                else if (dateOfBirth.getText().toString().isEmpty()) {
                    dateOfBirth.setError("Date of birth cannot be empty");
                }
                else if (licenseNumber.getText().toString().isEmpty()) {
                    licenseNumber.setError("License number cannot be empty");
                }
                else {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://10.0.2.2:8085")
                            .addConverterFactory(JacksonConverterFactory.create())
                            .client(httpClient)
                            .build();

                    HealthService healthService = retrofit.create(HealthService.class);

                    String nameString = name.getText().toString();
                    String emailString = email.getText().toString();
                    String dobString = dateOfBirth.getText().toString();
                    String phoneString = phoneNumber.getText().toString();
                    String licenseString = licenseNumber.getText().toString();

                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    Date dobDate = null;
                    try {
                        dobDate = format.parse(dobString);
                    } catch (ParseException e) {

                    }
                    int selected = gender.getCheckedRadioButtonId();
                    Gender g = selected == male.getId()?Gender.Male:Gender.Female;

                    String specialisationString;
                    int index = specialisations.getSelectedItemPosition();
                    switch (index) {
                        case 0:
                            specialisationString = "Imaging";
                            break;
                        case 1:
                            specialisationString = "Pediatrics";
                            break;
                        case 2:
                            specialisationString = "Orthopedic";
                            break;
                        case 3:
                            specialisationString = "Emergency";
                            break;
                        case 4:
                            specialisationString = "Obstetrics";
                            break;
                        case 5:
                            specialisationString = "Internal Medicine";
                            break;
                        case 6:
                            specialisationString = "Cardiology";
                            break;
                        case 7:
                            specialisationString = "Neuroscience";
                            break;
                        case 8:
                            specialisationString = "General Surgery";
                            break;
                        default:
                            specialisationString = "General Practice";
                    }

                    Doctor doctor = new Doctor(nameString, g, emailString, dobDate, phoneString, licenseString, specialisationString);
                    Call<Doctor> callPost = healthService.saveDoctor(doctor);
                    DoctorDataContainer.doctorNames.add(doctor.getName());
                    callPost.enqueue(new Callback<Doctor>() {
                        @Override
                        public void onResponse(Response<Doctor> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                Intent intent = new Intent(DoctorFormActivity.this, DoctorMainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(DoctorFormActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                Log.d("small fail", response.message());
                                Log.d("error no:", response.code() + "");
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            Toast.makeText(DoctorFormActivity.this, "Failed horribly", Toast.LENGTH_SHORT).show();
                            Log.d("big fail", throwable.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_doctor_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
