package org.bitbucket.infovillafoundation.javakid.healthclientandroid.patient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import org.bitbucket.infovillafoundation.javakid.healthclientandroid.R;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.Gender;
import org.bitbucket.infovillafoundation.javakid.healthclientandroid.main.HealthService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class PatientFormActivity extends AppCompatActivity {
    private TextView patientForm;
    private EditText name;
    private EditText nirc;
    private RadioGroup gender;
    private RadioButton male;
    private RadioButton female;
    private EditText email;
    private EditText dateOfBirth;
    private EditText phoneNumber;
    private Button finish;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_form);

        patientForm = (TextView) findViewById(R.id.patientForm);
        name = (EditText) findViewById(R.id.name);
        nirc = (EditText) findViewById(R.id.nirc);
        gender = (RadioGroup) findViewById(R.id.gender);
        male = (RadioButton) findViewById(R.id.male);
        female = (RadioButton) findViewById(R.id.female);
        email = (EditText) findViewById(R.id.email);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        finish = (Button) findViewById(R.id.finish);
        back = (Button) findViewById(R.id.backForm);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientFormActivity.this.finish();
            }
        });

        final OkHttpClient httpClient = new OkHttpClient();

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                if (name.getText().toString().isEmpty()) {
                    name.setError("Name cannot be empty");
                } else if (nirc.getText().toString().isEmpty()) {
                    nirc.setError("Nirc cannot be empty");
                } else if (gender.getCheckedRadioButtonId() == -1) {
                    male.setError("Gender cannot be empty");
                } else if (format.format(dateOfBirth.getText()).isEmpty()) {
                    dateOfBirth.setError("Date of birth cannot be empty");
                }
                else {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://10.0.2.2:8085")
                            .addConverterFactory(JacksonConverterFactory.create())
                            .client(httpClient)
                            .build();

                    HealthService healthService = retrofit.create(HealthService.class);

                    String nameString = name.getText().toString();
                    String nircString = nirc.getText().toString();
                    String emailString = email.getText().toString();
                    String dobString = dateOfBirth.getText().toString();
                    String phoneString = phoneNumber.getText().toString();
                    Date dobDate = null;
                    try {
                        dobDate = format.parse(dobString);
                    } catch (ParseException e) {

                    }
                    int selected = gender.getCheckedRadioButtonId();
                    Gender g = selected == male.getId() ? Gender.Male : Gender.Female;


                    Patient patient = new Patient(nameString, nircString, emailString, g, dobDate, phoneString);
                    PatientDataContainer.patientNames.add(patient.getName());
                    Call<Patient> callPost = healthService.savePatient(patient);
                    callPost.enqueue(new Callback<Patient>() {
                        @Override
                        public void onResponse(Response<Patient> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                Intent intent = new Intent(PatientFormActivity.this, PatientFormMainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(PatientFormActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                Log.d("small fail", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            Toast.makeText(PatientFormActivity.this, "Failed horribly", Toast.LENGTH_SHORT).show();
                            Log.d("big fail", throwable.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_booking_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
}
}
